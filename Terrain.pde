class Terrain {
  float rock, water, dirt, fsum, tfsum, twater, tdirt;
  float[][] flux;
  //PVector velocity;
  Terrain(float d, float w) {
    rock=max(0, d);
    water=max(0, w);
    dirt = 0;
    flux = new float[3][3];
    for (int x = 0; x<3; x++) {
      for (int y = 0; y<3; y++) {
        flux[x][y]=0;
      }
    }
    //tflux = new float[3][3];
    //for (int x = 0; x<3; x++) {
    //  for (int y = 0; y<3; y++) {
    //    tflux[x][y]=0;
    //  }
    //}
    fsum=0;
    tfsum=0;
    tdirt=dirt;
    twater=water;
  }
}
