/*erosion algo
 1. add water from sources, straightforward
 2. compute flow and velocity
 
 //
 //water pressure?
 wtrd = this.water+dirt+rock-that.water+dirt+rock
 c = area/length
 tflow[dirx][diry] = max(0,flow+watrd*c)
 
 //don't give out more water than available
 tfsum = [sum of tflow]
 k = min(1,water/tfsum) 
 flow[drx][dry] = tflow[drx][dry]*k
 
 fsum = [sum of flow] 
 
 
 3. compute erosion-sedimentation
 
 dirtcap = fsum*erosionc //get expected dirt amt
 dirtd = dirtcap-dirt //get expected dirt delta
 dirtd = rock-dirtd<0?rock:dirtd //if erosion takes all rock, take all
 dirtd = dirt+dirtd<0?dirt:dirtd //if deposit all sediment
 dirt = dirt + dirtd
 rock = rock - dirtd
 
 4. make dirt+water flow to lower places acording to flow[]
 velocity is somewhat preserved on flowing, reduced by friction
 velocity depends on water content, not water+dirt = self-limiting?
 
 wtrp = water/(water+dirt)
 for every flow neighbor:
 that.twater = this.tothat()*wtrp
 that.tdirt = this.tothat()*(1-wtrp)
 that.flow[tothat] = this.flow[tothat]*wtrp*flowconstant
 
 5. evaporate water, straightforward
 
 
 */

/*
todo:
 scale rain and evp to years
 compact evp to year change tick
 set edge case to ocean level 
 
 */


Terrain[][] world;
//float[][] wtrsrc;
float actualrain, evp;
float clz, erosionc, erosionslow, dirtcapc, dirtcapdc, dcc;
int stx, sty, simn, usbheight;
PrintWriter log;
boolean runsim, rain, enablerain, alwaysrain;
float wtramt, timescale, rainamt, sealevel, mtlevel, detail, drytimescale, initrainamt;
float dryarea, area, maxheight;
float maxslope, maxsingleslope;
int raintime, yearlength, year;
PFont font;
PGraphics grid;
int mult, gifdelay;
float wtrclr, dryclr;
boolean gify;

TerraGen tgen;
boolean donecopy;
float genscale;

void settings() {
  size(660, 660+60);
}

void setup() {



  mult=1;
  simn = 0;
  gifdelay = 3;
  wtrclr = 255/40.0;
  dryclr = 2;
  gify = true;

  int logsz = 60;
  usbheight = height-logsz;

  background(0);
  grid = createGraphics(width, usbheight);
  font = createFont("Consolas", 12);
  textFont(font);

  //log = createWriter("log.txt");

  clz = 1;
  stx=int(width/clz);
  sty=int(usbheight/clz);
  area = stx*sty;

  maxheight = 1000;
  dirtcapc = .9;
  dcc = dirtcapc;
  dirtcapdc = .10;
  timescale = .1;
  maxslope = maxheight*.2;
  maxsingleslope = maxheight*.035;


  yearlength = int(max(stx, sty));
  initrainamt = .035*165.0/yearlength;
  rainamt = initrainamt;

  erosionc = 165*4.0/yearlength;
  erosionslow = 165*.3/yearlength;

  year = 0;
  dryarea = 1;
  runsim = false;
  rain = true;
  raintime = yearlength;
  wtramt = 0;
  enablerain = true;
  alwaysrain = false;
  sealevel = .7;
  mtlevel = .985;
  drytimescale = 5;

  world = new Terrain[stx][sty];
  //wtrsrc = new float[stx][sty];

  //for (int x = 0; x<stx; x++) {
  //  for (int y = 0; y<sty; y++) {
  //    world[x][y] = new Terrain(map(x, 0, stx, 147, 150)+random(.1)+abs(sty/2.0-y)/50.0, 0);
  //    wtrsrc[x][y] = 0;
  //  }
  //}
  //for (int y = sty/2-1; y<sty/2; y++) {
  //  wtrsrc[stx-1][y] = 50;
  //}
  //for (int y = 0; y<sty; y++) {
  //  world[stx-1][y].rock = 0;
  //  world[stx-1][y].water = 140;
  //}
  //for (int x = stx-16; x<stx-7; x++) {
  //  world[x][sty/2-3].rock = 600;
  //  world[x][sty/2+3].rock = 600;
  //}
  //donecopy = true;
  //noise-based mapgen
  //
  detail = .004*clz;
  //
  //for (int x = 0; x<stx; x++) {
  //  for (int y = 0; y<sty; y++) {
  //    float r = noise(1, x*detail, y*detail);
  //    float g = noise(2, x*detail*2, y*detail*2)/2;
  //    float g1 = noise(3, x*detail*4, y*detail*4)/4;
  //    float g2 = noise(4, x*detail*8, y*detail*8)/8;
  //    float b = r+g+g1+g2;
  //    float dist = constrain((1/pow(2, .4))
  //      -(mag(x-(stx/2), y-(sty/2))/mag(stx/2, sty/2)), 0, 1);
  //    float h = map(b*b*dist, 0, 1, 0, 255);
  //    world[x][y] = new Terrain(h, max(0, (255*sealevel*sealevel)-h));
  //  }
  //}
  //for (int x = 0; x<stx; x++) {
  //  for (int y = 0; y<sty; y++) {
  //    wtrsrc[x][y] = rainamt*(1+noise(5, x*detail*clz*6, y*detail*clz*6)*sealevel*2);
  //    //wtrsrc[x][y] = noise(2, x*.1, y*.1)*rainamt;
  //  }
  //}
  //for (int i = 0;i<2500;i++){
  //  int wx =int(random(stx));
  //  int wy =int(random(sty));
  //  wtrsrc[wx][wy]+=7*rainamt*abs(randomGaussian());
  //}

  //islander mapgen
  genscale = 1;
  tgen = new TerraGen(int(stx/genscale), int(sty/genscale));
  donecopy = false;
}

void draw() {
  background(0);

  if (!tgen.running) {
    tgen.go();
  }

  if (tgen.done&&!donecopy) {
    float[][] genworld = tgen.world.clone();
    donecopy = true;

    float maxalt = 0;
    float[] alt = new float[stx*sty];
    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        alt[x+y*stx] = genworld[x][y];
      }
    }
    alt = sort(alt);
    maxalt = alt[alt.length-1];
    float genratio = (maxheight/maxalt);
    float startsealevel = alt[int(alt.length*mtlevel)]*genratio;
    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        float h = genworld[int(x/genscale)][int(y/genscale)]*genratio;
        world[x][y] = new Terrain(h, max(0, startsealevel-h));
        //wtrsrc[x][y] = rainamt
        //               //*(1+noise(5, x*detail*clz*6, y*detail*clz*6)*2)
        //               *sealevel
        //               *drytimescale;
        //wtrsrc[x][y] = noise(2, x*.1, y*.1)*rainamt;
      }
    }
    actualrain = rainamt*sealevel*drytimescale;
    evp = rainamt;
    runsim = true;
  }

  if (runsim&&frameCount%mult==0) {

    simn++;

    raintime--;

    if (year>30) {
      runsim = false;
      PGraphics out = createGraphics(stx, sty);
      out.beginDraw();
      out.background(0);
      float maxal = 0;
      for (int x = 0; x<stx; x++) {
        for (int y = 0; y<sty; y++) {
          maxal = max(maxal, world[x][y].dirt+world[x][y].rock);
        }
      }
      for (int x = 0; x<stx; x++) {
        for (int y = 0; y<sty; y++) {
          out.stroke((world[x][y].dirt+world[x][y].rock)*255.0/maxal);
          out.point(x, y);
        }
      }
      out.endDraw();
      out.save("out-final.png");
    }

    if (alwaysrain) {
      rain = true;
    }
    if (!alwaysrain&&raintime<=0) {
      boolean earlyero = erosionc!=erosionslow;
      if (enablerain) {
        rain = !rain;
        if (rain) {
          for (int x = 0; x<stx; x++) {
            for (int y = 0; y<sty; y++) {
              world[x][y].water = max(0, world[x][y].water-rainamt*yearlength*(drytimescale)*(earlyero?4:1));
            }
          }
          year++;
          dcc = dirtcapc+dirtcapdc*year;
          rainamt = map(year, 0, 30, initrainamt, 0)*(earlyero?1:1);
          wtramt = 0;
        }
        //raintime = int(yearlength/clz)+int(random(yearlength*.4/clz));
        raintime = int(yearlength*(earlyero?2:1));
        if (!rain) {          
          //raintime = 1;
          raintime = yearlength/2;
        }
        //raintime = 3;
      } else {
        rain = false;
      }
      if (year>3&&earlyero) {
        erosionc=erosionslow;
        rain = false;
        raintime =int(yearlength*.1);
        float[] alts = new float[stx*sty];

        for (int x = 0; x<stx; x++) {
          for (int y = 0; y<sty; y++) {
            alts[x+y*stx] = world[x][y].rock+world[x][y].dirt;
          }
        }
        alts = sort(alts);
        for (int x = 0; x<stx; x++) {
          for (int y = 0; y<sty; y++) {
            //world[x][y].water = max(0, world[x][y].water-rainamt*yearlength*30.0);
            world[x][y].water = max(0, alts[int(area*sealevel)]-(world[x][y].rock+world[x][y].dirt));
          }
        }
      }

      actualrain = rainamt*sealevel*drytimescale;
      evp = rainamt;
    }

    //twtramt = wtramt;

    //1
    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        Terrain ths = world[x][y];
        if (rain) {
          //float wt = max(actualrain, 0);
          //float wt = constrain(wtrsrc[x][y], 0, 1-ths.water);
          ths.water = max(0, ths.water+actualrain);
        }
        ths.tdirt = ths.dirt;
        ths.twater = ths.water;

        //println(world[x][y].water,wtrsrc[x][y]);
      }
    }
    wtramt += actualrain*stx*sty;

    //2 

    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        //2
        Terrain ths = world[x][y];
        ths.fsum=0;
        if (world[x][y].water>0) {
          for (int dir=0; dir<9; dir++) {
            PVector ndir = getneighbordir(dir);
            int nx = int(ndir.x);
            int ny = int(ndir.y);
            if (isInside(x+nx, y+ny)&&!(dir==4)) {
              Terrain that = world[x+nx][y+ny];
              float leng = mag(nx, ny);
              float thsz = max(0, ths.water+ths.dirt+ths.rock);
              float thtz = max(0, that.water+that.dirt+that.rock);
              float wtrd = thsz-thtz;
              //println(leng);
              ths.flux[nx+1][ny+1]=max(0, (ths.flux[nx+1][ny+1]*.99)+((wtrd/leng)*timescale));
            } else {
              //println(nx,ny,dir);
              ths.flux[nx+1][ny+1]=0;
            }
            //log.println(str(x)+" "+str(y)+" "+str(ths.flux[nx+1][ny+1])+" "+str(dir));

            ths.fsum+=ths.flux[nx+1][ny+1];
          }
          //println(ths.water);
          if (ths.fsum>ths.water&&!(ths.fsum==0)) { //limit flow to available water
            float k = constrain(ths.water/ths.fsum, 0, 1);
            //println(k,ths.water,ths.fsum);
            ths.fsum=0;
            for (int dir=0; dir<9; dir++) {
              PVector ndir = getneighbordir(dir);
              int nx = int(ndir.x);
              int ny = int(ndir.y);
              ths.flux[nx+1][ny+1]*=k;
              ths.fsum+=ths.flux[nx+1][ny+1];
            }
          }
        }
      }
    }

    //3
    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        Terrain ths = world[x][y];
        float dzm = 0;
        float dz = 0;
        float dzw = 0;
        for (int dir=0; dir<9; dir++) {
          PVector ndir = getneighbordir(dir);
          int nx = int(ndir.x);
          int ny = int(ndir.y);
          if (isInside(x+nx, y+ny)&&!(dir==4)) {
            Terrain that = world[x+nx][y+ny];
            float z = ths.dirt+ths.rock-(that.dirt+that.rock);
            dz += max(0, z);
            dzm = max(dzm, z);
            dzw += max(0, z+ths.water-that.water);
          }
        }
        //float dirtcap = max(0, log(ths.water)*ths.fsum*erosionc);
        float erode = pow(ths.fsum, dcc)*erosionc;
        float dirtcap = constrain(erode, max(0, dz-maxslope, dzm-maxsingleslope), max(dz, dzw));
        //float dirtcap = constrain(erode, 0, dz*ths.fsum);
        //println("dirtcap ",dirtcap,ths.water,ths.fsum);
        float dirtd = dirtcap-ths.dirt;
        //dirtd = dirtd>ths.rock?ths.rock:dirtd;
        //dirtd = dirtd<-ths.dirt?-ths.dirt:dirtd;
        dirtd*=timescale;
        dirtd = constrain(dirtd, -ths.dirt, ths.rock);
        ths.dirt+=dirtd;
        ths.rock-=dirtd;
        ths.tdirt = ths.dirt;
      }
    }

    //4
    for (int x = 0; x<stx; x++) {
      for (int y = 0; y<sty; y++) {
        Terrain ths = world[x][y];
        if (ths.water>0) {
          float wtrpart = ths.water/(ths.water+ths.dirt);
          wtrpart = constrain(wtrpart, 0, 1);
          //println(wtrpart, ths.water, ths.dirt);
          for (int dir = 0; dir<9; dir++) {
            PVector ndir = getneighbordir(dir);
            int dx = int(ndir.x);
            int dy = int(ndir.y);
            int nx = x+dx;
            int ny = y+dy;
            if (isInside(nx, ny)) {
              Terrain that = world[nx][ny];
              float wtrflux = ths.flux[dx+1][dy+1]*wtrpart;
              float drtflux = ths.flux[dx+1][dy+1]*(1-wtrpart);
              //float drtflux = ths.flux[dx+1][dy+1]*(wtrpart);
              that.twater += wtrflux;
              that.tdirt += drtflux;
              ths.twater -= wtrflux;
              ths.tdirt -= drtflux;
              //float flflux = ths.flux[dx+1][dy+1]*wtrpart*wtrpart*.012;
              float flflux = ths.flux[dx+1][dy+1]*.0012;
              that.flux[dx+1][dy+1] += flflux;
              ths.flux[dx+1][dy+1] -= flflux;
            }
          }
        }
      }
    }

    //5 

    float wtmin = 0;
    if (!rain) {
      for (int x = 0; x<stx; x++) {
        for (int y = 0; y<sty; y++) {
          Terrain ths = world[x][y];
          //ths.flux = ths.tflux.clone();
          ths.water= ths.twater;
          ths.dirt = ths.tdirt;
          //float wt = max(-ths.water, (-rainamt)*constrain(ths.water,0,1));
          float wt = max(-ths.water, -rainamt*.05);
          ths.water+=wt;
          wtmin+=wt;
        }
      }
    }
    wtramt += wtmin;

    //render


    if (simn%gifdelay==0) {
      dryarea = 1;
      grid.beginDraw();
      grid.noStroke();
      for (int x = 0; x<stx; x++) {
        for (int y = 0; y<sty; y++) {
          Terrain ths = world[x][y];
          boolean dry = ths.water<=0.001;
          float r = 0;
          float g = 0;
          float b = map(ths.fsum/2, 0, maxheight, 0, 255)+map(ths.water*wtrclr, 0, maxheight, 0, 255);
          if (dry) {
            dryarea++;
            //r = ((ths.rock/255.0)-sealevel)*(255.0/(1-sealevel));
            r = map((ths.rock/maxheight), 0, 1, 0, 255);
            g = map(ths.dirt, 0, maxheight, 0, 255);
          } else {
            r = map(ths.rock, 0, maxheight, 0, 255)+map(ths.dirt/2, 0, maxheight, 0, 255)-b;
            g = map(ths.dirt/2, 0, maxheight, 0, 255)+map(ths.fsum*wtrclr, 0, maxheight, 0, 255);
          }
          //println(x,y,r,g,b);
          grid.fill(r, g, b);
          //println(x*clz, y*clz, grid);
          grid.rect(x*clz, y*clz, clz, clz);
        }
      }
      grid.endDraw();
    }
  }


  image(grid, 0, 0);
  stroke(255);
  fill(255);
  int px = int(mouseX/clz);
  int py = int(mouseY/clz);
  if (isInside(px, py) && donecopy) {
    Terrain t = world[px][py];
    float dirtcap = max(0, t.fsum*erosionc);
    //float dirtcap = max(0, log(t.water)*t.fsum*erosionc);
    String l1 = str(px)+" "+str(py)+" | w: "+nfc(t.water, 2)+" r: "+nfc(t.rock, 2);
    l1 = l1+" d: "+nfc(t.dirt, 2);
    l1 = l1+" h: "+nfc(t.dirt+t.rock, 2);
    l1 = l1+" hw: "+nfc(t.dirt+t.rock+t.water, 2);
    l1 = l1+" dc: "+nfc(dirtcap, 3);
    l1 = l1+" arn: "+nfc(actualrain, 3);
    l1 = l1+" rn: "+nfc(rainamt, 3);
    String lf = "";
    for (int i = 0; i<9; i++) {
      lf+=(nfc(t.flux[i%3][i/3], 2)+(i%3==2?"|":" "));
    }
    text(l1, 10, height-50);
    text(lf+" sum "+nfc(t.fsum, 2), 10, height-35);
  }
  text(str(simn)+" "+str(frameCount)+" "+nfc(frameRate, 1)
    +" "+str(mult)+" "+(runsim?"Sim ":"Stop ")+(rain?"Rain":"")
    +" "+str(raintime)+" "+nfc(round(wtramt), 0)+" year: "+nfc(year, 0)
    +" dry: "+nfc((dryarea/(area*1.0))*100, 2)+"%", 
    10, height-20);

  //noLoop();
  if (gify&&runsim&&(simn%gifdelay==0)) {
    save("out_"+nf(simn/gifdelay, 6)+".png");
  }
}

void savegif() {
  //PImage gify = grid.copy();
  //gify.save("out_"+nf(simn/gifdelay, 4)+".png");
  save("out_"+nf(simn/gifdelay, 4)+".png");
}

void keyPressed() {
  if (key=='d') {
    runsim = !runsim;
  }
  if (key=='f') {
    enablerain = !enablerain;
  }
  if (key=='a') {
    mult=constrain(mult/2, 1, 1000);
  }
  if (key=='s') {
    mult=255;
  }
}


boolean isInside(int x, int y) {
  return !(x<0||y<0||x>=stx||y>=sty);
}


PVector getneighbordir(int a) {
  return new PVector((a%3)-1, (a/3)-1);
}
