public class TerraGen extends PApplet {
  TerraGen(int x, int y) {
    super();
    this.width = x;
    this.height = y;
    wx = x;
    wy = y;
    done = false;
    running = false;
  }

  void go() {
    if (!running) {
      running = true;

      PApplet.runSketch(new String[] {"Generating Terrain"}, this);
    }
  }

  int wx, wy;
  float [][] world;
  float dirtplaced, dirtexpected;
  ArrayList<Earth> earths;
  float clz;
  int stx, sty, usabley, area;
  PVector center;
  PFont font;
  boolean done, running;
  //float[] gaussianblur3x3, gaussianblur5x5;
  //int erosion = 0;
  //int erosionmax = 2;
  //float erosionpart = .08;
  //2 .09

  void settings() {
    size(wx, wy+20);
  }

  void setup() {
    background(0);
    noStroke();
    //gaussianblur3x3 = new float[]{1/16.0, 2/16.0, 1/16.0, 2/16.0, 4/16.0, 2/16.0, 1/16.0, 2/16.0, 1/16.0};
    //gaussianblur5x5 = new float[]{1, 4, 6, 4, 1, 4, 16, 24, 16, 4, 6, 24, 36, 24, 6, 4, 16, 24, 16, 4, 1, 4, 6, 4, 1};
    //for (int i = 0; i<gaussianblur5x5.length; i++) {
    //  gaussianblur5x5[i]=gaussianblur5x5[i]/256.0;
    //}
    font = createFont("Consolas", 18);
    textFont(font);
    clz = 1;
    usabley = height-20;
    stx = int(width/clz);
    sty = int(usabley/clz);
    area = stx*sty;
    center = new PVector(stx/2, sty/2);
    world = new float[stx][sty];
    float expected = 11;
    dirtplaced = 0;
    dirtexpected = area*expected;


    //pd = 0;
    //for (int x = 0; x<stx; x++) {
    //  for (int y = 0; y<sty; y++) {
    //    world[x][y] = 0;
    //  }
    //}
    earths = new ArrayList<Earth>();
    float spdia = 1/1.5;
    int nodeamt = max(1,max(stx,sty)/60);
    for (int i = 0; i<nodeamt; i++) {
      //earths.add(new Earth(stx/2, sty/2, area*1, true));
      earths.add(new Earth(int(random(stx*spdia)+stx*(1-spdia)/2), int(random(sty*spdia)+sty*(1-spdia)/2), area/nodeamt*expected*.8, true));
      //earths.add(new Earth(int(random(stx)), int(random(sty)), area/6.0, true));
    }
    for (int i = 0; i<nodeamt*25; i++) {
      //earths.add(new Earth(stx/2, sty/2, area*1, true));
      earths.add(new Earth(int(random(stx*spdia)+stx*(1-spdia)/2), int(random(sty*spdia)+sty*(1-spdia)/2), area/nodeamt*expected*(.2/25), true));
      //earths.add(new Earth(int(random(stx)), int(random(sty)), area/6.0, true));
    }
    //earths.add(new Earth(int(random(stx)), int(random(sty)), area*6, true));
    //earths.add(new Earth(int(random(stx)), int(random(sty)), area*6, true));
  }

  void draw() {

    float[][] newworld = world.clone();
    ArrayList<Earth> newert = new ArrayList<Earth>();
    //println(earths.size(), nfc(int(dirtplaced)), nfc(int(dirtplaced-pd)));
    //pd = dirtplaced;

    if (earths.size()==0) {


      float[] gaussianblur3x3, gaussianblur5x5;
      gaussianblur3x3 = new float[]{1/16.0, 2/16.0, 1/16.0, 2/16.0, 4/16.0, 2/16.0, 1/16.0, 2/16.0, 1/16.0};
      gaussianblur5x5 = new float[]{1, 4, 6, 4, 1, 4, 16, 24, 16, 4, 6, 24, 36, 24, 6, 4, 16, 24, 16, 4, 1, 4, 6, 4, 1};
      for (int i = 0; i<gaussianblur5x5.length; i++) {
        gaussianblur5x5[i]=gaussianblur5x5[i]/256.0;
      }
      int filtercount = 0;
      int kernelsize = 3;
      int offset = kernelsize/2;
      float[] kernel = (kernelsize==3?gaussianblur3x3:gaussianblur5x5);
      for (int i = 0; i<filtercount; i++) {
        float[][] smoothed = new float[stx][sty];
        for (int x = 0; x<stx; x++) {
          for (int y = 0; y<sty; y++) {
            float no = noise(x*.05, y*.05);
            for (int kx = 0; kx<kernelsize; kx++) {
              for (int ky = 0; ky < kernelsize; ky++) {
                int wx = constrain(x+kx-offset, 0, stx-1);
                int wy = constrain(y+ky-offset, 0, sty-1);
                int inde = ky+(kx*kernelsize);
                smoothed[wx][wy] += world[x][y]*lerp(kernel[inde], (inde==(kernelsize*kernelsize)/2?1:0), no);
              }
            }
          }
        }
        world = smoothed;
      }

      noLoop();
      done = true;
      //exit();
      return;
    }


    for (Earth er : earths) {
      if (er.dirt==0) {
        continue;
      }
      //float limit = 1/10.0;
      if (!isInside(er.x, er.y)) {
      //if (er.x<stx*limit||er.x>stx*(1-limit)||er.y<sty*limit||er.y>sty*(1-limit)) {
        er.x=int(random(stx));
        er.y=int(random(sty));
        //newert.add(er);
        dirtplaced+=er.dirt;
        continue;
      }

      int dx = 0;
      int dy = 0;
      float[] neighbors = new float[9];
      neighbors[0]=0;
      float[] priorities = new float[9];
      float priosum = 0;
      for (int i = 0; i < 9; i++) {
        int fx = (i%3)-1;
        int fy = (i/3)-1;
        int ex = er.x+fx;
        int ey = er.y+fy;
        if (!isInside(ex, ey)) {
          neighbors[i]=0.1;
        } else {
          neighbors[i]=world[ex][ey];
        }
      }
      float[] prios = new float[9];
      float myheight = neighbors[4];
      float climbingpower = log10(er.dirt)*1;
      for (int i = 0; i<9; i++) {
        float prio = max(myheight+climbingpower-neighbors[i], 0);
        prio *= prio;
        prios[i] = prio;
        priosum +=prio;
        priorities[i]= priosum;
      }
      float priority = random(priosum);
      for (int i = 0; i < 9; i++) {
        //println(priority, priorities[i], priosum, i);
        //println("[",i,"]", priority,priorities[i], prios[i], neighbors[i]);
        if (priority<priorities[i]) {
          dx = (i%3)-1;
          dy = (i/3)-1;
          //println("i moved!",dx,dy);
          break;
        }
      }

      boolean switchstar = random(1000000)>(er.star?600000:999999);
      er.star = switchstar?er.star:!er.star;
      boolean split = random(10000)>(er.star?9000:9990);
      if (split) {
        float newd = random(er.dirt/40);
        Earth nuer = new Earth(er.x+dx, er.y+dy, newd, er.star);
        er.dirt -= newd;
        newert.add(nuer);
      } else {
        //float d = abs(log10(er.dirt)/pow(world[er.x][er.y]+0.1, .7));
        float d = min(0.005+abs(log(er.dirt)), er.dirt);
        //float d = min(abs(0.0002+er.dirt*.02*(er.erode?randomGaussian():0.002)),er.dirt);
        //float d = abs(log10(er.dirt)*5);
        //println(d);
        dirtplaced+=d;
        newworld[er.x][er.y]+=d;
        er.dirt-=d;
      }

      fill(world[er.x][er.y]);
      rect(er.x*clz, er.y*clz, clz, clz);
      Earth nu = new Earth(er.x+dx, er.y+dy, er.dirt, er.star);
      newert.add(nu);
    }
    earths = newert;
    world = newworld;
    //textSize(15);
    //int mx = constrain(int(mouseX/clz), 0, stx-1);
    //int my = constrain(int(mouseY/clz), 0, sty-1);
    //fill(0);
    //float mouseheight = world[mx][my];
    //rect(10, height-50, max(textWidth(nfc(mouseheight, 2)), textWidth(str(mx)+str(my))), 42);
    //fill(255);
    //text(str(mx)+str(my), 10, height-30);
    //text(nfc(mouseheight, 2), 10, height-10);
    float r = dirtplaced/dirtexpected;
    fill(0);
    rect(0, usabley, width, 20);
    fill(0, r*255, 0);
    rect(0, usabley, width*r, 20);
    textAlign(CENTER, BOTTOM);
    fill(255);
    textSize(14);
    text(nfc(r*100, 2)+"%", width/2, height-4);
  }

  void exit()
  {
    dispose();
  }

  class Earth {
    float dirt;
    int x, y;
    boolean star;

    Earth(int xs, int ys, float dirts, boolean s) {
      this.x=xs;
      this.y=ys;
      this.dirt=dirts;
      this.star=s;
    }
  }

  float log10 (float x) {
    return (log(x) / log(10));
  }

  boolean isInside(int x, int y) {
    return !(x<0||y<0||x>=stx||y>=sty);
  }
}
